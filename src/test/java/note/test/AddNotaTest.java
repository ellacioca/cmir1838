package note.test;

import static org.junit.Assert.*;

import note.model.Nota;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import note.utils.ClasaException;
import note.utils.Constants;

import note.controller.NoteController;

public class AddNotaTest {
	
	private NoteController ctrl;
	
	@Before
	public void init(){
		ctrl = new NoteController("n.txt");
	}
	
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	@Test
	public void test1() throws ClasaException {
		Nota nota = new Nota(1, "Desen", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test2() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(10.1, "Istorie", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test3() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(0, "Istorie", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test4() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		Nota nota = new Nota(1, "Isto", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test5() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(1, "Istorie", 5.002);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test6() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(1, "Istorie", 11);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test7() throws ClasaException {
		Nota nota = new Nota(2, "Istorie", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test8() throws ClasaException {
		Nota nota = new Nota(1000, "Istorie", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test9() throws ClasaException {
		Nota nota = new Nota(999, "Istorie", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test10() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(1001, "Istorie", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test11() throws ClasaException {
		Nota nota = new Nota(1000, "Desena", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test12() throws ClasaException {
		Nota nota = new Nota(1000, "DesenDesenDesenDesen", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test13() throws ClasaException {
		Nota nota = new Nota(1000, "DesenDesenDesenDese", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	
	@Test
	public void test14() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		Nota nota = new Nota(1, "DesenDesenDesenDesenD", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test15() throws ClasaException {
		Nota nota = new Nota(1000, "DesenDesenDesenDese", 1);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test16() throws ClasaException {
		Nota nota = new Nota(1000, "DesenDesenDesenDese", 2);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test17() throws ClasaException {
		Nota nota = new Nota(1000, "DesenDesenDesenDese", 9);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test18() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(1, "Istorie", 0);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test19() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		Nota nota = new Nota(1, "", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test20() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		Nota nota = new Nota(1, "a", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test21() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(1, "Istorie", -2);
		ctrl.addNota(nota);
	}
	
	@Test
	public void tes22() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(-1001, "Istorie", 5);
		ctrl.addNota(nota);
	}

	//----------------------------------------------------------------------------------------------
	@Test
	public void test23() throws ClasaException {
		Nota nota = new Nota(2, "Matematica", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test24() throws ClasaException {
		Nota nota = new Nota(6, "Informatica", 5);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test25() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(12.44, "Matematica", 10);
		ctrl.addNota(nota);
	}

	@Test
	public void test26() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		Nota nota = new Nota(2, "4", 10);
		ctrl.addNota(nota);
	}

	@Test
	public void test27() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(9, "Romana", 2.68);
		ctrl.addNota(nota);
	}

	@Test
	public void test28() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(1589, "Informatica", 9);
		ctrl.addNota(nota);
	}

	@Test
	public void test29() throws ClasaException {
		Nota nota = new Nota(1, "Materia", 4);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test30() throws ClasaException {
		Nota nota = new Nota(1000, "Materia", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test31() throws ClasaException {
		Nota nota = new Nota(156, "Materia", 9);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test32() throws ClasaException {
		Nota nota = new Nota(745, "Materia", 8);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test33() throws ClasaException {
		Nota nota = new Nota(52, "Istorie", 4);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test34() throws ClasaException {
		Nota nota = new Nota(478, "Fizica", 6);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test35() throws ClasaException {
		Nota nota = new Nota(51, "Biologie", 5);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test36() throws ClasaException {
		Nota nota = new Nota(520, "Romana", 8);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test37() throws ClasaException {
		Nota nota = new Nota(369, "Materie", 4);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test38() throws ClasaException {
		Nota nota = new Nota(524, "Materie", 5);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test39() throws ClasaException {
		Nota nota = new Nota(12, "Materie", 9);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test40() throws ClasaException {
		Nota nota = new Nota(854, "Materie", 4);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}


}
